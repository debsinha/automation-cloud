from typing import Any, Optional
from uuid import uuid4
import requests

from django.core.files.base import ContentFile
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, response
from ...core import JobStatus
from ..base_plugin import BasePlugin
from .utils import start_wf, stop_wf, update_component,get_component_table


class BAARPlugin(BasePlugin):
    PLUGIN_ID = "baar"
    PLUGIN_NAME = "BAAR"
    DEFAULT_ACTIVE = True
    PLUGIN_DESCRIPTION = "Built-in saleor plugin that handles invoice creation."

    def _acao_response(response):
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'GET'

    def RunWF(self,wf_name, *components) -> Any:
        status = start_wf(wf_name)
        return status

    def StopWF(self, wf_name) -> Any:
        status = stop_wf(wf_name)
        return status

    def UpdateComponent(self, component) -> Any:
        pass

    def webhook(self, request: WSGIRequest, path: str, previous_value) -> HttpResponse:
        print(request.FILES)
        if "/start" in path:
            status_code = start_wf(path.split("/")[-1])
        elif "/stop" in path:
            status_code = stop_wf(path.split("/")[-1])
        elif path == "/update":
            status_code = update_component(request)
        elif "/components" in path:
            response = JsonResponse(data=get_component_table(path.split("/")[-1].replace("_"," ")))
            response['Access-Control-Allow-Origin'] = '*'
            response['Access-Control-Allow-Methods'] = 'GET'
            return response
        response = JsonResponse(data={"status":status_code})
        response['Access-Control-Allow-Origin'] = '*'
        response['Access-Control-Allow-Methods'] = 'GET'
        return response
