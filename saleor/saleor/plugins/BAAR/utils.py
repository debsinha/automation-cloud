from datetime import datetime
from django.conf import settings
import requests
import json
import pandas as pd
import io
from django.db import connection
import psycopg2
import json
from requests.models import Response
# settings.configure()

# BAAR_host = settings.BAAR_HOST
# BAAR_port = settings.BAAR_PORT
# BAAR_tenant = settings.BAAR_TENANT
# BAAR_user = settings.BAAR_USER
# BAAR_key = settings.BAAR_KEY

BAAR_host = "18.221.78.83"
BAAR_port = "81"
BAAR_tenant = "test1"
BAAR_user = "admin"
BAAR_key = "dfd07f597b998eed55b54c966390d345"

def get_player_wf(wf_name):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessView?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
    response = request.json()
    for inst in response:
        if inst["workflowProcessModelVo"]["name"] == wf_name:
            return inst["workflowProcessModelVo"]["id"]
    return "Not Found"

def get_servicetask_record():
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/ServiceTaskClassRecordMethodMappingView?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
    response = request.json()
    if response:
        return response
    return "Unable to get Service Task Class Mapping"

def find_running_wf(wf_id):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessExecutionView/" + str(wf_id) + "?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
    response = request.json()
    for inst in response:
        if inst["workflowProcessExecutionModelVo"]["running"] == "true":
            return inst["workflowProcessExecutionModelVo"]["id"]
    return False

def db_query(query, table, db_type):
    query_post = {"query": query, "tenantName": BAAR_tenant, "dbType": db_type, "db": BAAR_tenant + "_baar", "tableName": table}
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/integrator/FetchData?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""}, data=json.dumps(wf_post))
    response = request.json()

def start_wf(wf_name):
    wf_id = get_player_wf(wf_name)
    wf_mapping = get_servicetask_record()
    wf_post = {}
    if wf_name and wf_id and wf_mapping:
        wf_post["workflowInstanceId"] = wf_id
        wf_post["variables"] = {}
        wf_post["variables"]["tenant"] = BAAR_tenant
        wf_post["variables"]["workflowName"] = wf_name
        wf_post["variables"]["recordMethodMappingList"] = wf_mapping
        request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessRun?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""}, data=json.dumps(wf_post))
        print(request.text)

def stop_wf(wf_name):
    wf_id = get_player_wf(wf_name)
    status = find_running_wf(wf_id)
    if status != False:
        request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessExecutionDelete/" + str(status) + "?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
        response = request.text
        if response == "true":
            return "Successfully Stopped Workflow"
        else:
            return "Unable to Stop Workflow"
    return "Workflow is not running or not found"

def get_component_instance(name):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataView?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
    response = request.json()
    for instance in response:
        if instance["instanceName"] == name:
            # instance["activeFlag"] = "true"
            # for i in range(len(instance["componentInstanceMappingList"])):
            #     instance["componentInstanceMappingList"][i]["activeFlag"] = "true"
            return instance
    return "Not Found"

def get_component_data(instance_id):
        request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataViewById/" + str(instance_id) + "?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""})
        response = request.json()
        return response

def clear_component(instance):
    instance["componentInstanceInventoryList"] = []
    instance = str(instance).replace("'",'"')
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataSave?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""}, data=instance)
    response = request.text
    print(response)

def generate_component(file):
    data = pd.read_excel(file)
    component_list = []
    count = 1
    for index, row in data.iterrows():
        row_dict = {}
        row_dict["rowId"] = count
        row_dict["componentInstanceInventoryMap"] = {}
        for column in row.iteritems():
            row_dict["componentInstanceInventoryMap"][column[0]] = {}
            row_dict["componentInstanceInventoryMap"][column[0]]["inventoryParamName"] = column[0]
            row_dict["componentInstanceInventoryMap"][column[0]]["inventoryParamRowid"] = count
            row_dict["componentInstanceInventoryMap"][column[0]]["inventoryParamValue"] = column[1]
            row_dict["componentInstanceInventoryMap"][column[0]]["inventoryParamDisplayName"] = "null"
            row_dict["componentInstanceInventoryMap"][column[0]]["activeFlag"] = "Y"
        count = count + 1
        component_list.append(row_dict)
    return component_list

def update_component(name):
    instance_id = get_component_instance(list(name.FILES)[0])["instanceId"]
    data_instance = get_component_data(instance_id)
    clear_component(data_instance)
    data = generate_component(name.FILES[list(name.FILES)[0]])
    data_instance["componentInstanceInventoryList"] = data
    data_instance = str(data_instance).replace("'",'"')
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataSave?tenant=" + BAAR_tenant, headers={"Username":BAAR_user, "Content-Type":"application/json", "API-KEY":BAAR_key, "X-NO-LOGIN-MODE":""}, data=data_instance)
    response = request.text
    print(response)
    return response

def get_component_table(name):
    component_json = {}
    conn = psycopg2.connect(host="localhost", database="saleor", user="saleor", password="saleor")
    cursor = conn.cursor()    
    cursor.execute("select component from product_component where product='" + name + "'")
    row = cursor.fetchall()
    c = 0
    if row:
        component_json["count"] = len(row)
        component_json["table"] = []
        for r in row:
            data = {}
            data["id"] = c
            data["component"] = r[0]
            component_json["table"].append(r[0])
            c = c + 1
    
    print(json.loads(json.dumps(component_json)))
    return json.loads(json.dumps(component_json))
if __name__ == "__main__":
    get_component_table("Phishing Email")
    instance_id = get_component_instance("PhishingEmail")["instanceId"]
    data_instance = get_component_data(instance_id)
    clear_component(data_instance)



