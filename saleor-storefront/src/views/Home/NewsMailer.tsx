import React from 'react';
import {Row,Col,Form,Button} from 'react-bootstrap'

const NewsMailer: React.FC=()=>{
    return(
        <div className="mail-work">
            <Row>
            <Col lg={7} xs={12} md={6}>
            <div className="mail-work1">
            <h2><strong><em>Register With Us</em></strong></h2>
            <Form>
                <Form.Group controlId="formBasicEmail">
                <Form.Label><strong>Email*</strong></Form.Label>
                <Form.Control  className="form-email"  type="email" placeholder="Your email"  autoComplete="on" />
                </Form.Group>

                <Button style={{marginTop:"10px",marginBottom:'10px'}} variant ="primary" type="submit">Submit</Button>
                <p>Register with us and be the first one to know about the upcoming automations</p>
            </Form>     
            </div>
            </Col>
            </Row>  
  </div>
    )
}
export default NewsMailer;