import React,{useState} from 'react';
import {Carousel,Row,Container,Col} from 'react-bootstrap'
// import {account1} from './Account.json'
// import image from "../../images/account2.jpg"
import './scss/index.css'
import account from '../../images/account2.jpg'


const Testimonials: React.FC=()=>{
    
        // const [index, setIndex] = useState(0);
      
        // const handleSelect = (selectedIndex, e) => {
        //   setIndex(selectedIndex);
        // };  
      
        return (
            <>
            <Container>
              <Row>
                <h1 style={{textAlign:"center"}}>Testimonials</h1>
          
                    <Carousel>
                   <Carousel.Item>
                  <div style={{display:"flex",justifyContent:'center',padding:"85px"}}>
                   <img
                     className="f-block"
                     src={account}
                     alt="First slide"
                   />
                  
                     <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</p> 
                     </div>
                 </Carousel.Item>
                 <Carousel.Item>  
                 <div style={{display:"flex",justifyContent:'center',padding:"95px"}}>
                   <img
                     className="f-block"
                     src={account}
                     alt="Second slide"
                   />
                      <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown type specimen book. It has survived not</p>
                    </div>
                 </Carousel.Item>
                 <Carousel.Item>
                 <div style={{display:"flex",justifyContent:'center',padding:"95px"}}>
                 <img
                     className="f-block"
                     src={account}
                     alt="Third slide"
                   />
              
                      <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</p>
                   </div> 
                 </Carousel.Item>
              
           </Carousel>  
        
                  
          </Row>
          </Container>
          
          </>
        );
      
}
export default Testimonials;