import "./scss/index.scss";
import "./scss/index.css"

import classNames from "classnames";
import * as React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Link } from "react-router-dom";

import { Loader, ProductsFeatured } from "../../components";
import { generateCategoryUrl } from "../../core/utils";
// import Carousel from 'react-bootstrap/Carousel'

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Container,Row,Col,Carousel} from "react-bootstrap";
import image1 from "../../images/products1.jpg"
import image2 from "../../images/products2.jpg"
import image3 from "../../images/products3.jpg"

import {
  ProductsList_categories,
  ProductsList_shop,
  ProductsList_shop_homepageCollection_backgroundImage,
} from "./gqlTypes/ProductsList";

import { structuredData } from "../../core/SEO/Homepage/structuredData";

import noPhotoImg from "../../images/no-photo.svg";
import NewsMailer from "./NewsMailer";
import Testimonials from "./Testimonials";

const Page: React.FC<{
  loading: boolean;
  categories: ProductsList_categories;
  backgroundImage: ProductsList_shop_homepageCollection_backgroundImage;
  shop: ProductsList_shop;
}> = ({ loading, categories, backgroundImage, shop }) => {
  const categoriesExist = () => {
    return categories && categories.edges && categories.edges.length > 0;
  };
  const intl = useIntl();
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  
  return (
    <>
      <script className="structured-data-list" type="application/ld+json">
        {structuredData(shop)}
      </script>
      {/* console.log("Hello this is me"); */}
      
      {/* <div
        className="home-page__hero"
        style={
          backgroundImage
            ? { backgroundImage: `url(${backgroundImage.url})` }
            : null
        }
      >
        <div className="home-page__hero-text">
          <div>
            <span className="home-page__hero__title">
              <h1>
                <FormattedMessage defaultMessage="Final reduction" />
              </h1>
            </span>
          </div>
          <div>
            <span className="home-page__hero__title">
              <h1>
                <FormattedMessage defaultMessage="Up to 70% off sale" />
              </h1>
            </span>
          </div>
        </div>
        <div className="home-page__hero-action">
          {loading && !categories ? (
            <Loader />
          ) : (
            categoriesExist() && (
              <Link
                to={generateCategoryUrl(
                  categories.edges[0].node.id,
                  categories.edges[0].node.name
                )}
              >
                <Button testingContext="homepageHeroActionButton">
                  <FormattedMessage defaultMessage="Shop sale" />
                </Button>
              </Link>
            )
          )}
        </div>
      </div> */}

     

      <div>
             {/* carousel main home Page */}
              <Carousel  fade={true} pause={false} interval={1000}>
          <Carousel.Item>
            <img
              className="d-block "
              src={image1}
              alt="First slide"
            />
            {/* <Carousel.Caption>
              <h3>First slide label</h3>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption> */}
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block "
              src={image2}
              alt="Second slide"
            />

            {/* <Carousel.Caption>
              <h3>Second slide label</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption> */}
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block "
              src={image3}
              alt="Third slide"
            />

            {/* <Carousel.Caption>
              <h3>Third slide label</h3>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption> */}
          </Carousel.Item>
        </Carousel>
      </div>
      <ProductsFeatured
        title={intl.formatMessage({ defaultMessage: "Featured Automation" })}
      />
      {categoriesExist() && (
        <div className="home-page__categories">
          <div className="container">
            <h3>
              <FormattedMessage defaultMessage="Shop by Automations" />
            </h3>

            <div style={{padding:"4%"}}>
            
            <Slider {...settings}>
              {categories.edges.map(({ node: category }) => (
                
                <div key={category.id}>
                    
                  <Link
                    to={generateCategoryUrl(category.id, category.name)}
                    key={category.id}
                  >
                    <div
                      className={classNames(
                        "home-page__categories__list__image",
                        {
                          "home-page__categories__list__image--no-photo": !category.backgroundImage,
                        }
                      )}
                      style={{
                        backgroundImage: `url(${
                          category.backgroundImage
                            ? category.backgroundImage.url
                            : noPhotoImg
                        })`,
                      }}
                    /> 
                    <h3>{category.name}</h3>
                  </Link>
                </div> 
              ))}
            </Slider>
            </div>
          </div>
        </div>
      )}

      <section id="#How-it-works">
          <div>
              <h1 style={{textAlign:"center"}}>How it Works ? </h1>

              <div>
                <Container>
                    <Row>
                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <span className="count">1</span>
                      <h2 className="count-text">Select Automation</h2>
                      </div>
                      </Col>

                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <span className="count">2</span><br/>
                      <h2 className="count-text">Configure</h2>
                      </div>
                      </Col>

                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <span className="count">3</span> <br/>
                      <h2 className="count-text">Use</h2>
                      </div>
                      </Col>
                      </Row>
                </Container>
              </div>
          </div>
      </section>

      <section id='#testimonials' style={{marginTop:"80px"}}>
              <Testimonials/>
      </section>

      <section id="#request-emails">
           <NewsMailer/>
      </section>

      
    </>
  );
};

export default Page;
