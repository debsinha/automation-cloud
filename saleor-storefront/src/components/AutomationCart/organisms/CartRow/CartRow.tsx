import React, { useEffect, useState  } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Link } from "react-router-dom";
import {useHistory} from "react-router-dom";
import {Row,Col} from "react-bootstrap";
import axios from "axios";

// import { Icon, IconButton } from "@components/atoms";
import { CachedImage, TextField } from "@components/molecules";
import { commonMessages } from "@temp/intl";

// import { generateProductUrl } from "../../../../core/utils";

import { generateAutomationUrl,generateBAARUrl } from "../../../../core/utils"

import * as S from "./styles";
import { IProps } from "./types";
import {Button } from "react-bootstrap";
import Test1 from "../../../Test/Test1"

// const QuantityButtons = (
//   add: () => void,
//   subtract: () => void,
//   index?: number
// ) => (
//   <S.QuantityButtons data-test="quantityControls">
//     <div onClick={subtract} data-test="subtractButton">
//       <Icon size={16} name="horizontal_line" />
//     </div>
//     <div onClick={add} data-test="increaseButton">
//       <Icon size={16} name="plus" />
//     </div>
//   </S.QuantityButtons>
// );

/**
 * Product row displayed on cart page
 */
 

export const CartRow: React.FC<IProps> = ({
  // index,
  // totalPrice,
  // unitPrice,
  name,
  sku,
  quantity,
  // maxQuantity,
  onQuantityChange,
  thumbnail,
  attributes = [],
  onRemove,
  id,
}: IProps) => {
  const [tempQuantity, setTempQuantity] = useState<string>(quantity.toString());
  // const [isTooMuch, setIsTooMuch] = useState(false);
  // const [count, setCount] = useState(0)
  const [counter, setCounter] = useState(0)
  const [table, setTable] = useState([])
  const intl = useIntl();
  const history=useHistory()

  const handleChange1=()=>{
    axios.get('http://18.221.78.83:9000/plugins/baar/components/'+baar)
    .then((res)=>{
     console.log(res);
     const data2=res.data.count;
     const tableData=res.data.table
     setCounter(data2); 
     setTable(tableData);
      // history.push('/testpage')
    })
    .catch(err=>console.log(err))
    
   }
   console.log("mandown",counter,table);
   
  // const handleBlurQuantityInput = () => {
  //   let newQuantity = parseInt(tempQuantity, 10);

  //   if (isNaN(newQuantity) || newQuantity <= 0) {
  //     newQuantity = quantity;
  //   } else if (newQuantity > maxQuantity) {
  //     newQuantity = maxQuantity;
  //   }

  //   if (quantity !== newQuantity) {
  //     onQuantityChange(newQuantity);
  //   }

  //   const newTempQuantity = newQuantity.toString();
  //   if (tempQuantity !== newTempQuantity) {
  //     setTempQuantity(newTempQuantity);
  //   }

  //   setIsTooMuch(false);
  // };

//   useEffect(() => {
//     axios.get('http://18.221.78.83:9000/plugins/baar/components/'+baar)
//     .then((res)=>{
//      console.log(res);
//      const data1=res.data.count;
//      console.log("this is the effect of Baar",data1);
//      setCount(data1)
//     })
//     .catch(err=>console.log(err))
//   },[]);
// console.log("raw data",count);

  // const add = React.useCallback(
  //   () => quantity < maxQuantity && onQuantityChange(quantity + 1),
  //   [quantity]
  // );
  // const subtract = React.useCallback(
  //   () => quantity > 1 && onQuantityChange(quantity - 1),
  //   [quantity]
  // );
  // const handleQuantityChange = (evt: React.ChangeEvent<any>) => {
  //   const newQuantity = parseInt(evt.target.value, 10);

  //   setTempQuantity(evt.target.value);

  //   setIsTooMuch(!isNaN(newQuantity) && newQuantity > maxQuantity);
  // };

  // const quantityErrors = isTooMuch
  //   ? [
  //       {
  //         message: intl.formatMessage(commonMessages.maxQtyIs, { maxQuantity }),
  //       },
  //     ]
  //   : undefined;

  // const productUrl = generateProductUrl(id, name);

  const automationUrl= generateAutomationUrl(id,name);

  const baar=generateBAARUrl(name);

  const handleClick1=()=>{
    axios.get('http://18.221.78.83:9000/plugins/baar/start/'+baar)
    .then((res)=>{
     console.log(res);
     alert(res.request.status)
    })
    .catch(err=>console.log(err))
  }

  const handleClick2=()=>{
    axios.get('http://18.221.78.83:9000/plugins/baar/stop/'+baar)
    .then((res)=>{
     console.log(res);
     alert(res.request.status)
    })
    .catch(err=>console.log(err))
  }
  // const onRemove =()=>{
  //   alert("remove me")
  // }

  // console.log("Real name",name.toLowerCase());
  return (
    <S.Wrapper data-test="cartRow" data-test-id={sku}>
      <S.Photo>
        
          <CachedImage data-test="itemImage" {...thumbnail} />
        
      </S.Photo>
      <S.Description>
        
          <S.Name data-test="itemName">{name}</S.Name>
        
        <S.Sku>
          <S.LightFont>
            <FormattedMessage {...commonMessages.sku} />:{" "}
            <span data-test="itemSKU">{sku || "-"}</span>
          </S.LightFont>
        </S.Sku>
        <S.Attributes data-test="itemAttributes">
          {attributes.map(({ attribute, values }, attributeIndex) => (
            <S.SingleAttribute key={attribute.id}>
              <span
                data-test="itemSingleAttribute"
                data-test-id={attributeIndex}
              >
                <S.LightFont>{attribute.name}:</S.LightFont>{" "}
                {values.map(value => value.name).join(", ")}
              </span>
            </S.SingleAttribute>
          ))}
        </S.Attributes>
      </S.Description>

      <S.Trash>
        {/* <IconButton
          testingContext="removeButton"
          testingContextId={sku}
          size={22}
          name="trash"
          onClick={onRemove}
        /> */}
        <section>
              <div style={{marginRight:'15px',marginBottom:'15px'}}>
                    <Row>
                        <Col>
                            <Button onClick={handleClick1}  variant="primary">Start</Button>
                        </Col>

                        <Col>
                            <Button onClick={handleClick2}  variant="danger">Stop</Button>
                        </Col>
                    </Row>   
              </div>
        </section>

        <Button onClick={handleChange1}
        variant="outline-primary" size="lg" block>    
        Configure
        </Button>
      </S.Trash>
      {/* <New count={data1}/> */}
      {/* <S.Quantity>
        <TextField
          name="quantity"
          label={intl.formatMessage(commonMessages.qty)}
          value={tempQuantity}
          onBlur={handleBlurQuantityInput}
          onChange={handleQuantityChange}
          contentRight={QuantityButtons(add, subtract, index)}
          errors={quantityErrors}
        />
      </S.Quantity> */}
      

      {/* <S.TotalPrice>
        <S.PriceLabel>
          <S.LightFont>
            <FormattedMessage {...commonMessages.totalPrice} />:
          </S.LightFont>
        </S.PriceLabel>
        <p data-test="totalPrice">{totalPrice}</p>
      </S.TotalPrice>
      <S.UnitPrice>
        <S.PriceLabel>
          <S.LightFont>
            <FormattedMessage {...commonMessages.price} />:
          </S.LightFont>
        </S.PriceLabel>
        <p data-test="unitPrice">{unitPrice}</p>
      </S.UnitPrice> */}

      <Test1 c={counter}  tab={table}/>
    </S.Wrapper>
    
  );
};
