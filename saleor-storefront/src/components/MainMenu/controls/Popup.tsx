import React from "react";
import { Dialog, DialogTitle, DialogContent,Typography } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import CloseIcon from '@material-ui/icons/Close';
import Controls from './Controls'
import Button from '@material-ui/core/Button';

interface PopupProps {
  openPopup: boolean;
  
  setOpenPopup: (arg0: boolean) => void;
  
}
const useStyles = makeStyles((theme: Theme) =>
createStyles({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
      flexDirection: 'row',
      display:'flex',
    },
  },
 
}),
);
export const Popup = ({ openPopup, setOpenPopup }: PopupProps): JSX.Element => {
 
const classes = useStyles();
  return (
    <Dialog style={{margin:"0px 2rem 0px"}} open={openPopup}>
      <DialogTitle>
        <div style={{display:'flex'}}>
             <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
                      Automation
              </Typography>
          <Controls.ActionButton
              color="secondary"
              onClick={()=>{setOpenPopup(false)}}>
              <CloseIcon />
          </Controls.ActionButton>
        </div>
      
      </DialogTitle>
      <DialogContent >
        <div>
            <form className={classes.root} noValidate autoComplete="off">
            <TextField type="text" id="outlined-basic" label="Name" variant="filled" fullWidth autoFocus />
            {/* <TextField type="text" id="outlined-basic"  label="Description" variant="filled" /> */}
            {/* <TextareaAutosize
              maxRows={10}
              aria-label="maximum height"
              placeholder="Maximum 10 rows"
              defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                  ut labore et dolore magna aliqua."
            /> */}
            <textarea id="w3review" name="w3review" rows="14" cols="50" defaultValue="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of L">
            
            </textarea>
            <TextField type="email" id="outlined-basic" label="Email Address" fullWidth variant="filled" />
            <Button style={{marginLeft:"22px"}} type="submit" variant="contained" color="primary">
              Submit
            </Button>
            </form>
        </div>
         
        {/* <button
          onClick={() => {
            setOpenPopup(false);
          }}
        >
          Close Card   
        </button> */}
      </DialogContent>
    </Dialog>
  );
};

export default Popup;
